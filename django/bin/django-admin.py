#!/usr/bin/env python

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "..")))
sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "..", "..")))
sys.path.insert(0, os.path.abspath(os.getcwd()))


from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
