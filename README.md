Sortir à Mâcon
================

Site web d'actualités - loisirs à Mâcon.


Installation d'un environnement de dev
--------------------------------------

**Prérequis :**

* Python 2.7
* pip


### Créer un virtualenv avec python 2.7

    virtualenv --no-site-package -p /usr/bin/python2.7 venv
    source venv/bin/activate


### Clone du code source 

    mkdir src
    git clone git@bitbucket.org:askinteractive/sortiramacon.git
    cd src
    git checkout develop
    

### Tree du projet 
    
    projet : sortiramacon/sortiramacon
    applications annexes : sortiramacon/apps
    django : sortiramacon/django
    cms : sortiramacon/zebnetcms


### Installation des dépendances

Dépendances locales : en utilisateur restreint

    pip install -r requirements.txt


### Initialiser la base de données
    
    cd sortiramacon
	./manage.py makemigrations
    ./manage.py migrate


### Lancer le serveur de dev
    ./manage.py runserver

Visiter http://localhost:8000/

