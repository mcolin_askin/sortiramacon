# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *
from django.conf import settings
from sortiramacon.news.sitemaps import NewsSitemap
from sortiramacon.events.sitemaps import EventsSitemap
from sortiramacon.movies.sitemaps import MoviesSitemap
from sortiramacon.restaurants.sitemaps import RestaurantsSitemap
from sortiramacon.associations.sitemaps import AssociationsSitemap

from cms.sitemaps import CMSSitemap

sitemaps = {
    'news': NewsSitemap, 
    'events': EventsSitemap,
    'movies': MoviesSitemap,
    'restaurants': RestaurantsSitemap,
    'associations': AssociationsSitemap,
    'pages': CMSSitemap
}



urlpatterns = patterns('',
    url(r'^actualites/', include('sortiramacon.news.urls')),
    url(r'^evenements/', include('sortiramacon.events.urls')),
    url(r'^lieux/', include('sortiramacon.events.place_urls')),
    
    url(r'^restaurants/', include('sortiramacon.restaurants.urls')),
    url(r'^associations/', include('sortiramacon.associations.urls')),

    url(r'^films/', include('sortiramacon.movies.urls')),

    
    (r'^rechercher/', include('haystack.urls')),
    

    
    (r'^sitemap-(?P<section>.+)\.xml$', 'sortiramacon.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

    url(r'^sitemap-pages.xml$', 'sortiramacon.sitemaps.views.sitemap', {'sitemaps': {'pages': CMSSitemap}}),

    url(r'^sitemap.xml$', 'sortiramacon.sitemaps.views.index',  {'sitemaps': sitemaps}),

    url(r'^', include('zebnetcms.urls')),

)

if settings.DEBUG:
    urlpatterns = patterns('',
        (r'^' + settings.MEDIA_URL.lstrip('/'), include('appmedia.urls')),
    ) + urlpatterns