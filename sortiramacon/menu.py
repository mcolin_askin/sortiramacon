# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from admin_tools.menu import items, Menu

from django.conf import settings

from cms.models import Page

class ZebnetCMSMenu(Menu):
    """
    Custom Menu for pact admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index'), css_classes=("dashboard",)),
            
            items.MenuItem(_('Plan du site'), reverse('admin:cms_page_changelist'), css_classes=("projects",)),
            
            # Ajout des pages
            
        ]
        
        pages = []
        
        pages.append(items.MenuItem(
                    title='Ajouter une page', 
                    url=reverse('admin:cms_page_add'),
                    #css_classes=("add",),
        ))
        
        for page in Page.objects.all_root():
            if page.children.all().count() > 0:
                children = []
                
                children.append(items.MenuItem(
                    title='Ajouter une page', 
                    url=reverse('admin:cms_page_add') + '?target=%d&position=last-child' % page.id,
                    #css_classes=("add",),
                ))
                for child in page.children.all():
                    children.append(items.MenuItem(
                            title=child.get_title(), 
                            url=reverse('admin:cms_page_change', args=[child.id])
                    ))
                pages.append(items.MenuItem(
                            title=page.get_title(), 
                            url=reverse('admin:cms_page_change', args=[page.id]),
                            children=children
                ))
            else:
                pages.append(items.MenuItem(
                            title=page.get_title(), 
                            url=reverse('admin:cms_page_change', args=[page.id])
                ))
        
        self.children.append(
            items.MenuItem(
                title='Gestion des pages',
                children=pages,
                css_classes=("pages",)
            ),
        )
        
        # Gestion des actualites
        if 'sortiramacon.news' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Actualités',
                    children=[
                        items.MenuItem(
                            title=u'Ajouter une actualité',
                            url='/admin/news/news/add/',
                            css_classes=("add",)),
                        items.MenuItem(
                            title=u'Gestion des actualités',
                            url='/admin/news/news/'),
                        items.MenuItem(
                            title=u'Gestion des catégories',
                            url='/admin/news/category/'),
                    ],
                    css_classes=("articles",)
                ),
            )
        
        # Gestion des evenements
        if 'sortiramacon.events' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Événements',
                    children=[
                        items.MenuItem(
                            title=u'Ajouter un événement',
                            url='/admin/events/event/add/',
                            css_classes=("add",)),
                        items.MenuItem(
                            title=u'Gestion des événements',
                            url='/admin/events/event/'),
                    ],
                    css_classes=("events",)
                ),
            )
            self.children.append(
                items.MenuItem(
                    title=u'Lieux',
                    children=[
                        items.MenuItem(
                            title=u'Ajouter un lieu',
                            url='/admin/events/place/add/',
                            css_classes=("add",)),
                        items.MenuItem(
                            title=u'Gestion des lieux',
                            url='/admin/events/place/'),
                    ],
                    css_classes=("places",)
                ),
            )
        
        # Gestion des restaurants
        if 'sortiramacon.restaurants' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Restaurants',
                    children=[
                        items.MenuItem(
                            title=u'Ajouter un restaurant',
                            url='/admin/restaurants/restaurant/add/',
                            css_classes=("add",)),
                        items.MenuItem(
                            title=u'Gestion des restaurants',
                            url='/admin/restaurants/restaurant/'),
                        items.MenuItem(
                            title=u'Gestion des spécialités',
                            url='/admin/restaurants/restaurantcategory/'),
                    ],
                    css_classes=("restaurants",)
                ),
            )

        # Gestion des films
        if 'sortiramacon.movies' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Films',
                    children=[
                        items.MenuItem(
                            title=u'Ajouter un film',
                            url='/admin/movies/movie/add/',
                            css_classes=("add",)),
                        items.MenuItem(
                            title=u'Gestion des films',
                            url='/admin/movies/movie/'),
                    ],
                    css_classes=("events",)
                ),
            )

        
        # Gestion des associations
        if 'sortiramacon.associations' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Associations',
                    children=[
                        items.MenuItem(
                            title=u'Ajouter une association',
                            url='/admin/associations/association/add/',
                            css_classes=("add",)),
                        items.MenuItem(
                            title=u'Gestion des associations',
                            url='/admin/associations/association/'),
                        items.MenuItem(
                            title=u'Gestion des thématiques',
                            url='/admin/associations/associationcategory/'),
                    ],
                    css_classes=("associations",)
                ),
            )
            
        
        # Gestion des formulaires
        if 'form_designer' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Formulaires',
                    children=[
                        items.MenuItem(
                            title=u'Journal des formulaires',
                            url='/admin/form_designer/formlog/',
                            #css_classes=("add",)
                        ),
                        items.MenuItem(
                            title=u'Gestion des formulaires',
                            url='/admin/form_designer/formdefinition/',
                        )
                    ],
                    css_classes=("forms",)
                ),
            )
        # Gestion des média
        if 'image_filer' in settings.INSTALLED_APPS:
            self.children.append(
                items.MenuItem(
                    title=u'Biliothèque d\'images',
                    url='/admin/image_filer/folder/',
                    css_classes=("gallery",)
                ),
            )
        
        
        
    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        # Test if superuser to add apps list
        request = context['request']
        user = request.user
        if user.is_superuser and user.id == 1:
            self.children += [
                items.AppList(
                    _('Administration'),
                    css_classes=("settings",)
                ),
            ]
        
