# -*- coding: utf-8 -*-
import re
import calendar
import time
from datetime import date, datetime, timedelta

from django import template
from django.template import defaultfilters
from django.contrib.sites.models import Site

from django.conf import settings

register = template.Library()

def get_sidebar_calendar():
    from events.models import Event
    from events.views import mnames
    
    current_date = date.today()
    year, month = current_date.year, current_date.month
    
    # init variables
    cal = calendar.Calendar()
    
    month_days          = cal.itermonthdays(year, month)
    nyear, nmonth, nday = time.localtime()[:3]
    
    lst = [[]]
    week = 0
    
    # make month lists containing list of days for each week
    # each day tuple will contain list of entries and 'current' indicator
    for day in month_days:
        entries = current = False   # are there entries for this day; current day?
        if day:
            entries = Event.objects.filter(start_date__year=year, start_date__month=month, start_date__day=day)
            if day == nday and year == nyear and month == nmonth:
                current = True
        if day < 10:
            n_day = "0%s" % day
        else:
            n_day = "%s" % day
        
        lst[week].append((day, entries, current, n_day))
        if len(lst[week]) == 7:
            lst.append([])
            week += 1
    month_number=month
    if month < 10:
        month_number = "0%d" % month
    else:
        month_number = "%d" % month
    
    next_date = current_date + timedelta(days=31)
    if next_date.month < 10:
        n_month_n = "0%s" % next_date.month
    else:
        n_month_n = "%s" % next_date.month
    next = {
        "year" : next_date.year,
        "month": next_date.month,
        "month_name": mnames[next_date.month-1],
        "month_n": n_month_n
    }
    
    previous_date = current_date - timedelta(days=31)
    if previous_date.month < 10:
        p_month_n = "0%s" % previous_date.month
    else:
        p_month_n = "%s" % previous_date.month
    previous = {
        "year" : previous_date.year,
        "month": previous_date.month,
        "month_name": mnames[previous_date.month-1],
        "month_n": p_month_n
    }
    return {"year":  year, "month": month, "month_days": lst, 
            "mname": mnames[month-1], "month_n": month_number,
            "next": next, "previous": previous}
get_sidebar_calendar = register.inclusion_tag('events/small_calendar.html')(get_sidebar_calendar)

def get_category_list():
    from news.models import Category
    
    categories = Category.objects.all().order_by("id")
    
    return {"categories":  categories}
get_category_list = register.inclusion_tag('category_list.html')(get_category_list)
