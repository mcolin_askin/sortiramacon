/*
 * Periodic Custom jQuery Javascripts
 * Copyright (c) 2010 We Are Pixel8
 *
 * IMPORTANT NOTE: You may customize and edit any of the code below. The groups of code above this section are minified versions of various jQuery plug ins and we recommend not editing above this line unles you are a comfortable doing so.
 *
 */
 
// Equal height columns
function equalHeight(group) {
    tallest = 0;
    group.each(function() {
        thisHeight = jQuery(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.height(tallest);
}

// Animated scrolling
jQuery.fn.anchorAnimate = function(settings) {

 	settings = jQuery.extend({
		speed : 400
	}, settings);	
	
	return this.each(function(){
		var caller = this
		jQuery(caller).click(function (event) {	
			event.preventDefault()
			var locationHref = window.location.href
			var elementClick = jQuery(caller).attr("href")
			
			var destination = jQuery(elementClick).offset().top;
			jQuery("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, settings.speed, function() {
				window.location.hash = elementClick
			});
		  	return false;
		})
	})
}

jQuery(document).ready(function() {

	// Toggle Popular Posts Sticky Header
	jQuery('.hide-sticky').click(function() {
		jQuery('#sticky-header').slideToggle(300);
		jQuery(this).toggleClass('show-sticky'); return false;
	});

	// Scroll to top
	jQuery('.scroll-top').click(function() {
		jQuery('html, body').animate({ scrollTop:0 });
		return false;
	});
	
	// Animated Scroll Effects
	jQuery('a.scroll-it').anchorAnimate();
	
	// Utility Banner Menu
	jQuery('#utility-banner ul.header-menu').supersubs({ 
            minWidth:    10,   // minimum width of sub-menus in em units 
            maxWidth:    30,   // maximum width of sub-menus in em units 
            extraWidth:  1,    // extra width can ensure lines don't sometimes turn over 
                               // due to slight rounding differences and font-family 
        }).superfish({
			delay: 200,
            animation: {opacity:'show',height:'show'},
            speed: 'fast',
            dropShadows: false,
            autoArrows: false
            
	}); 
	
	// Main Navigation Menu
	jQuery('#mainnav-bar ul').supersubs({ 
            minWidth:    15,   // minimum width of sub-menus in em units 
            maxWidth:    30,   // maximum width of sub-menus in em units 
            extraWidth:  1,    // extra width can ensure lines don't sometimes turn over 
                               // due to slight rounding differences and font-family 
        }).superfish({
			delay: 200,
            animation: {opacity:'show',height:'show'},
            speed: 'fast',
            dropShadows: false,
            autoArrows: false
            
	});

	// Featured Slider
	jQuery('#featured-slides').cycle({
		fx:					'fade',
		speed:				400,
		timeout:			10000,
		cleartypeNoBg:		true,
		pager:				'#slide-thumbs',
		pagerAnchorBuilder: function(idx, slide) { 
        	// return selector string for existing anchor 
        	return '#slide-thumbs li:eq(' + idx + ') a'; 
    		}
	});
	
	// Fancybox video player
	jQuery('.play-fancybox').click(function() {
		$.fancybox({
			'padding'			: 0,
			'autoScale'			: false,
			'transitionIn'		: 'elastic',
			'transitionOut'		: 'elastic',
			'speedIn'			: 400, 
			'speedOut'			: 200,
			'title'				: this.title,
			'width'				: 680,
			'height'			: 495,
			'href'				: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'				: 'swf',
			'swf'				: {
			'wmode'				: 'transparent',
			'allowfullscreen'	: 'true'
			}
		});
		return false;
	});
	
	// Add Fancybox to anchors containing .jpg, .jpeg, .gif, .png
	jQuery('a[href$=.jpg],a[href$=.jpeg],a[href$=.png],a[href$=.gif]').fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	400, 
		'speedOut'		:	200
	});


	// Sidebar Tabs	
	jQuery('#tab-container').tabs({
		fx: {
			height: 'toggle',
			opacity: 'toggle'
			}
		});
	
	// Initiate Equal Height Columns
    equalHeight(jQuery('#archived-posts .post-archive'));
    equalHeight(jQuery('.related li'));
    
    // Category Links Animation
    jQuery('.category-link-list a').hover(function() {
    	jQuery(this).animate({paddingLeft: '10px'}, 200);
    	}, function() {
    	jQuery(this).animate({paddingLeft: '0'}, 200);
    });
    
    // Home Page Category Links Animation
    jQuery('.headlines h2 a').hover(function() {
    	jQuery(this).animate({paddingRight: '16px'}, 200);
    	}, function() {
    	jQuery(this).animate({paddingRight: '8px'}, 200);
    });
    
    // Popular Post Slider
    jQuery('#trending-wrapper').cycle({
		fx:					'scrollHorz',
		prev:				'#prev-trending',
		next:				'#next-trending',
		speed:				400,
		cleartypeNoBg:		true,
		timeout:			0
	});
	
	// Popular Post Caption
	jQuery('.sticky-wrap').hover(function(){  
		jQuery('.sticky-text', this).stop().animate({top:'5px'},{queue:false,duration:300});  
		}, function() {  
		jQuery('.sticky-text', this).stop().animate({top:'110px'},{queue:false,duration:300});  
		});  

});