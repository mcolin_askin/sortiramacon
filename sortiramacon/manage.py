#!/usr/bin/env python

import os, sys
import sys

# 1. Including Apps directory
sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "..", "apps")))
# 2. Including Root Directory
sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "..")))
# 3. Including Project Directory
sys.path.insert(0, os.path.abspath(os.getcwd()))

# from django.core.management import execute_manager
try:
    import settings  # Assumed to be in the same directory.
except ImportError:
    import sys

    sys.stderr.write(
        "Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized "
        "things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does "
        "indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)

# if __name__ == "__main__":
#     execute_manager(settings)


#   For any version > 1.4
#
# #!/usr/bin/env python
# import os, sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ project_name }}.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
