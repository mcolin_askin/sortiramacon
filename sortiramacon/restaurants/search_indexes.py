# -*- coding: utf-8 -*-

from haystack import indexes
from cms_search.search_helpers.indexes import MultiLanguageIndex

from restaurants.models import Restaurant

class RestaurantIndex(indexes.SearchIndex, indexes.Indexable):
    text  = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    url   = indexes.CharField(stored=True, model_attr="slug")
    
    def get_model(self):
        return Restaurant
    
    def prepare_url(self, obj):
        return obj.get_absolute_url()
    
    class HaystackTrans:
        fields = ('url', 'title')