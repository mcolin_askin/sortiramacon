from django.contrib.sitemaps import Sitemap
from .models import Restaurant

class RestaurantsSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Restaurant.published.all()

    def lastmod(self, obj):
        return obj.updated