from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from restaurants.models import LatestRestaurantsPlugin, Restaurant
from events import settings

from django.conf import settings

class CMSLatestRestaurantsPlugin(CMSPluginBase):
    """ Plugin class for the latest news """
    model = LatestRestaurantsPlugin

    name = _('Latest events')
    render_template = 'restaurants/latest_restaurants.html'
    admin_preview = False
    
    def render(self, context, instance, placeholder, extra=None):
        """ Render the latest news """
        query = Restaurant.published
        
        if instance.category is None:
            latest = query.all()[:instance.limit]
        else:
            latest = query.filter(category=instance.category).all()[:instance.limit]
        
        if instance.template is not None and instance.template != "":
            self.render_template = instance.template
        
        context.update({
            'instance': instance,
            'latest': latest,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSLatestRestaurantsPlugin)
