from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class RestaurantsApphook(CMSApp):
    name = _("Latest restaurants")
    urls = ["restaurants.urls"]

apphook_pool.register(RestaurantsApphook)
