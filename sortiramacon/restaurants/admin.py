from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext

from restaurants.forms import RestaurantForm, RestaurantCategoryForm
from restaurants.models import Restaurant, RestaurantPhoto, RestaurantCategory


class RestaurantCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'order')
    form = RestaurantCategoryForm


class RestaurantPhotoAdminInline(admin.TabularInline):
    model = RestaurantPhoto


class RestaurantAdmin(admin.ModelAdmin):
    """ Admin for events """
    
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('slug', 'title', 'category','is_published',)
    list_filter = ('is_published', 'category',)
    search_fields = ['title', 'content']
    form = RestaurantForm
    
    #filter_horizontal = ['news', ]
    
    actions = ['make_published', 'make_unpublished']
    
    save_as = True
    save_on_top = True
    
    inlines = [RestaurantPhotoAdminInline,]
    
    def queryset(self, request):
        """ Override to use the objects and not just the default visibles only. """
        return Restaurant.objects.all()
    
    def make_published(self, request, queryset):
        """ Marks selected news items as published """
        rows_updated = queryset.update(is_published=True)
        self.message_user(request, ungettext('%(count)d restaurant was published', 
                                             '%(count)d restaurants where published', 
                                             rows_updated) % {'count': rows_updated})
    make_published.short_description = _('Publish selected restaurants')
    
    def make_unpublished(self, request, queryset):
        """ Marks selected news items as unpublished """
        rows_updated =queryset.update(is_published=False)
        self.message_user(request, ungettext('%(count)d restaurant was unpublished', 
                                             '%(count)d restaurants where unpublished', 
                                             rows_updated) % {'count': rows_updated})
    make_unpublished.short_description = _('Unpublish selected restaurants')


admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(RestaurantCategory, RestaurantCategoryAdmin)
