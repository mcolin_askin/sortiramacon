from django.conf.urls.defaults import *

urlpatterns = patterns('restaurants.views',
    # By category --------
    
    url(
        r'^$', 
        'archive_index', 
        name='restaurant_archive_index'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/$', 
        'archive_category_index', 
        name='restaurant_archive_category_index'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$',
        'object_detail', 
        name='restaurant_detail'
    ),
)

