# Create your views here.
from django.http import Http404

from django.views.generic import list_detail

from restaurants.models import Restaurant
from restaurants.models import RestaurantCategory

def archive_index(request):
    
    queryset = RestaurantCategory.objects.all()
    
    
    return list_detail.object_list(
                        request,
                        queryset,
                        paginate_by=50)


def archive_category_index(request, category):
    cat = RestaurantCategory.objects.get(slug=category)
    
    queryset = Restaurant.published.filter(category=cat)
    
    
    return list_detail.object_list(
                        request,
                        queryset,
                        paginate_by=15)

def object_detail(request, category, slug):
    queryset = Restaurant.published.all()
    
    return list_detail.object_detail(
                        request,
                        queryset,
                        slug=slug)
