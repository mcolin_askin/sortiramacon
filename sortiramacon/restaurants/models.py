# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.conf import settings

from cms.models import CMSPlugin


class RestaurantCategory(models.Model):
    """
    Category
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    slug            = models.SlugField(_('Slug'))
    
    content         = models.TextField(_('content'), blank=True)
    
    order           = models.IntegerField(_('order'), default=20)
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ('order', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('restaurant_archive_category_index', (), {
            'category': self.slug,
            })


class PublishedRestaurantsManager(models.Manager):
    """ Filters out all unpublished and items with a publication date in the future. """
    
    def get_query_set(self):
        return super(PublishedRestaurantsManager, self).get_query_set().filter(is_published=True)


class Restaurant(models.Model):
    """ Restaurant model """
    
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), unique=True, 
                            help_text=_('A slug is a short name which uniquely identifies the news item for this day'))
    
    
    phone   = models.CharField(_('phone'), blank=True, max_length=80)
    address = models.TextField(_('address'), blank=True)
    website = models.CharField(_('website'), blank=True, max_length=250)
    email   = models.CharField(_('email'), blank=True, max_length=80)
    
    content = models.TextField(_('Content'), blank=True)
    
    image  = models.ImageField(_('Image'), upload_to="restaurants/", null=True, blank=True)
    
    category = models.ForeignKey(RestaurantCategory, related_name="r_category", verbose_name=_("Category"))
    
    is_published = models.BooleanField(_('Published'), default=False)
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    
    published = PublishedRestaurantsManager()
    objects   = models.Manager()
    
    
    class Meta:
        verbose_name = _('Restaurant')
        verbose_name_plural = _('Restaurants')
        ordering = ('title', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('restaurant_detail', (), {
            'category': self.category.slug,
            'slug': self.slug,
            })


class LatestRestaurantsPlugin(CMSPlugin):
    """ Model for the settings when using the latest news cms plugin. """
    
    category = models.ForeignKey(RestaurantCategory, null=True, blank=True, default=None)
    
    limit    = models.PositiveIntegerField(_('Number of items to show'), 
               help_text=_('Limits the number of items that will be displayed'))


class RestaurantPhoto(models.Model):
    """
    Photo for restaurant
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    image           = models.ImageField(_('Image'), upload_to="restaurants/photos/",)
    
    order           = models.IntegerField(_('order'), default=20)
    
    restaurant      = models.ForeignKey(Restaurant, verbose_name=_('restaurant'))
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    
    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Potos')
        ordering = ('order', 'created')
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'

