# -*- coding: utf-8 -*-
import os
import sys

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
# 1. Including Apps directory
# sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "..", "apps")))
sys.path.insert(0, os.path.join(PROJECT_PATH, "..", "apps"))
# 2. Including Root Directory
sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "..")))
# 3. Including Project Directory
sys.path.insert(0, os.path.abspath(os.getcwd()))

gettext = lambda s: s

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('zebNet', 'sriguet@zebnet.fr'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'clients_sortiramacon',
        'USER': 'sortiramacon',
        'PASSWORD': 'vNhgW2sA',
        'HOST': '',
        'PORT': '',
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/admin/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'w79y)2vx5#ugf7#+iat&_b(=@3l!3*!mojys9khyr-18#@=yre'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'cms.context_processors.media',
)

MIDDLEWARE_CLASSES = (

    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.media.PlaceholderMediaMiddleware',
)

ROOT_URLCONF = 'sortiramacon.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, "templates"),
    os.path.join(PROJECT_PATH, "..", "zebnetcms", "templates")
)

INSTALLED_APPS = (
    # 'raven.contrib.django.raven_compat',

    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',

    'sorl.thumbnail',

    # CMS core part
    'cms',
    'mptt',
    'menus',
    'south',
    'appmedia',
    'publisher',

    'haystack',
    'cms_search',

    # CMS Plugins
    'cms.plugins.text',
    'cms.plugins.picture',
    # 'cms.plugins.link',
    'cms.plugins.file',
    'cms.plugins.snippet',
    'cms.plugins.googlemap',

    'form_designer',

    'zebnetcms',

    'sortiramacon.news',
    'sortiramacon.events',
    'sortiramacon.restaurants',
    'sortiramacon.associations',
    'sortiramacon.movies',

    'sortiramacon',
)

RAVEN_CONFIG = {
    'dsn': 'http://f07c220503c54762a42255c8edc963a2:8671f659d99f43fab081132c48af4320@sentry.askin.fr:9000/26',
}

# Settings for CMS part

CMS_URL_OVERWRITE = True
CMS_MENU_TITLE_OVERWRITE = True
CMS_SEO_FIELDS = True
CMS_FLAT_URLS = False
CMS_SHOW_END_DATE = False
CMS_SHOW_START_DATE = False
CMS_REDIRECTS = True

CMS_MEDIA_URL = MEDIA_URL + "cms/"

CMS_TEMPLATES = (
    ('right-column.html', u"Colonne à droite"),
    ('default.html', u'Pleine page'),
    ('homepage.html', u"Page d'accueil"),
    #    ('right-column.html', u"Colonne à droite"),
)

LANGUAGES = [
    ('fr', u'Français'),
]

ADMIN_TOOLS_MENU = 'sortiramacon.menu.ZebnetCMSMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'zebnetcms.dashboard.ZebnetCMSIndexDashboard'

CMS_APPLICATIONS_URLS = (
    ('sortiramacon.news.urls', 'News'),
)
CMS_NAVIGATION_EXTENDERS = (
    ('sortiramacon.news.navigation.get_nodes', 'News navigation'),
)

CMS_PLACEHOLDER_CONF = {
    'content': {
        # 'text_only_plugins': ('Form',)
        'name': gettext("Content"),
    },
    'right_bar': {
        "extra_context": {"width": 290},
        'name': gettext(u"Colonne de droite"),
    },
    'left_bar': {
        'name': gettext(u"Colonne de gauche"),
    },
    'first_line': {
        'name': gettext(u"Première ligne"),
    },
    'second_line': {
        'name': gettext(u"Deuxième ligne"),
    },
    'third_line': {
        'name': gettext(u"Troisième ligne"),
    },
    'slider': {
        'name': gettext(u"Carroussel de news"),
    },
}

GOOGLE_ANALYTICS_API_LOGIN = 'contact@zebnet.fr'
GOOGLE_ANALYTICS_API_PASSWORD = 'zebnetaskinteractive'
GOOGLE_ANALYTICS_API_ACCOUNT = 'www.sortiramacon.com'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}

try:
    from local_settings import *
except ImportError:
    pass
