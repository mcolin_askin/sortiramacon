from django.conf.urls.defaults import *

from news.models import News

news_info_dict = {
    'queryset': News.published.all(),
    'date_field': 'pub_date',
}

news_info_month_dict = {
    'queryset': News.published.all(),
    'date_field': 'pub_date',
    'month_format': '%m',
}

urlpatterns = patterns('django.views.generic.date_based',
    (r'^$', 'archive_index', news_info_dict, 'news_archive_index'),
    
    (r'^(?P<year>\d{4})/$',
     'archive_year', news_info_dict, 'news_archive_year'),
    
    (r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
     'archive_month', news_info_month_dict, 'news_archive_month'),
    
    (r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
     'archive_day', news_info_month_dict, 'news_archive_day'),
)

urlpatterns += patterns('news.views',
    # By category --------
    
    url(
        r'^(?P<category>[-\w]+)/$', 
        'archive_category_index', 
        name='news_archive_category_index'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/$',
        'archive_category_year', 
        name='news_archive_category_year'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/$',
        'archive_category_month', 
        name='news_archive_category_month'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
        'archive_category_day', 
        name='news_archive_category_day'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        'object_detail', 
        name='news_detail'
    ),
)

