# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin

from django.conf import settings


class PublishedNewsManager(models.Manager):
    """ Filters out all unpublished and items with a publication date in the future. """
    
    def get_query_set(self):
        return super(PublishedNewsManager, self)\
                            .get_query_set()\
                            .filter(is_published=True)\
                            .filter(pub_date__lte=datetime.datetime.now())


class News(models.Model):
    """ News model """
    
    title = models.CharField(_('Title'), max_length=255)
    
    slug = models.SlugField(_('Slug'), unique_for_date='pub_date', 
                            help_text=_('A slug is a short name which uniquely identifies the news item for this day'))
    
    
    excerpt = models.TextField(_('Excerpt'), blank=True)
    content = models.TextField(_('Content'), blank=True)
    
    image  = models.ImageField(_('Image'), upload_to="news/")
    
    category = models.ForeignKey("Category", related_name="n_category", verbose_name=_("Category"))
    
    is_published = models.BooleanField(_('Published'), default=False)
    is_featured  = models.BooleanField(_('Featured'), default=False)
    pub_date     = models.DateTimeField(_('Publication date'), 
                                        default=datetime.datetime.now())
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    
    
    published = PublishedNewsManager()
    objects   = models.Manager()
    
    
    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ('-pub_date', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('news_detail', (), {
            'category': self.category.slug,
            'year': self.pub_date.strftime("%Y"),
            'month': self.pub_date.strftime("%m"),
            'day': self.pub_date.strftime("%d"),
            'slug': self.slug,
            })
    
    def get_next(self):
        try:
            return News.published.filter(pub_date__gt=self.pub_date)[0]
        except:
            return None
    
    def get_previous(self):
        try:
            return News.published.filter(pub_date__lt=self.pub_date).order_by('-pub_date')[0]
        except:
            return None


class Category(models.Model):
    """
    Category
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    slug            = models.SlugField(_('Slug'))
    
    content         = models.TextField(_('content'), blank=True)
    
    pub_date        = models.DateTimeField(_('Publication date'), 
                                           default=datetime.datetime.now())
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    classname       = models.CharField(_('class name'), 
                                       blank=True, max_length=80, default="")
    
    
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ('-pub_date', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'


class LatestNewsPlugin(CMSPlugin):
    """ Model for the settings when using the latest news cms plugin. """
    
    category = models.ForeignKey(Category, null=True, blank=True, default=None)
    featured = models.BooleanField(_('featured'), default=False)
    
    limit    = models.PositiveIntegerField(_('Number of items to show'), 
               help_text=_('Limits the number of items that will be displayed'))
    
    template = models.CharField(_('template'), blank=True, max_length=255, null=True)



class NewsPhoto(models.Model):
    """
    Photo for news
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    image           = models.ImageField(_('Image'), upload_to="events/",)
    
    order           = models.IntegerField(_('order'), default=20)
    
    news            = models.ForeignKey(News, verbose_name=_('event'))
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    
    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Potos')
        ordering = ('order', 'created')
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'

