from django.contrib.sitemaps import Sitemap
from .models import News

class NewsSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return News.published.all()

    def lastmod(self, obj):
        return obj.updated

    def publication_date(self, obj):
        return obj.pub_date

    def keywords(self, obj):
        return obj.category

    def get_urls(self, page=1, site=None):
        if site is None:
            if Site._meta.installed:
                try:
                    site = Site.objects.get_current()
                except Site.DoesNotExist:
                    pass
            if site is None:
                raise ImproperlyConfigured("In order to use Sitemaps you must either use the sites framework or pass in a Site or RequestSite object in your view code.")

        urls = []
        for item in self.paginator.page(page).object_list:
            loc = "http://%s%s" % (site.domain, self.__get('location', item))
            priority = self.__get('priority', item, None)
            url_info = {
                'location':   loc,
                'lastmod':    self.__get('lastmod', item, None),
                'changefreq': self.__get('changefreq', item, None),
                'priority':   str(priority is not None and priority or ''),
                'publication_date':    self.__get('publication_date', item, None),
                'keywords':    self.__get('keywords', item, None),
            }
            urls.append(url_info)
        return urls

    def __get(self, name, obj, default=None):
        try:
            attr = getattr(self, name)
        except AttributeError:
            return default
        if callable(attr):
            return attr(obj)
        return attr