# Create your views here.
from django.http import Http404

from django.views.generic import date_based
from django.views.generic import list_detail

from news.models import News
from news.models import Category

def archive_category_index(request, category):
    cat = Category.objects.get(slug=category)
    
    queryset = News.published.filter(category=cat)
    
    date_field = "pub_date"
    
    return list_detail.object_list(
                        request,
                        queryset,
                        paginate_by=10,
                        extra_context={"category": cat})


def archive_category_year(request, category, year):
    try:
        cat = Category.objects.get(slug=category)
        
        queryset = News.published.filter(category=cat)
        
        date_field = "pub_date"
        
        return date_based.archive_year(
                            request,
                            year,
                            queryset,
                            date_field)
    except:
        raise Http404

def archive_category_month(request, category, year, month):
    try:
        cat = Category.objects.get(slug=category)
        
        queryset = News.published.filter(category=cat)
        
        date_field = "pub_date"
        
        return date_based.archive_month(
                            request,
                            year,
                            month,
                            queryset,
                            date_field,
                            '%m')
    except:
        raise Http404


def archive_category_day(request, category, year, month, day):
    try:
        cat = Category.objects.get(slug=category)
        
        queryset = News.published.filter(category=cat)
        
        date_field = "pub_date"
        
        return date_based.archive_day(
                            request,
                            year,
                            month,
                            day,
                            queryset,
                            date_field,
                            '%m')
    except:
        raise Http404

def object_detail(request, category, year, month, day, slug):
    queryset = News.published.all()
    date_field = "pub_date"
    
    return date_based.object_detail(
                        request,
                        year,
                        month,
                        day,
                        queryset,
                        date_field,
                        '%m',
                        slug=slug)
