from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from associations.models import LatestAssociationPlugin, Association
from associations import settings

from django.conf import settings

class CMSLatestAssociationsPlugin(CMSPluginBase):
    """ Plugin class for the latest news """
    model = LatestAssociationPlugin

    name = _('Latest associations')
    render_template = 'associations/latest_associations.html'
    admin_preview = False
    
    def render(self, context, instance, placeholder, extra=None):
        """ Render the latest news """
        query = Association.published
        
        if instance.category is None:
            latest = query.all()[:instance.limit]
        else:
            latest = query.filter(category=instance.category).all()[:instance.limit]
        
        if instance.template is not None and instance.template != "":
            self.render_template = instance.template
        
        context.update({
            'instance': instance,
            'latest': latest,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSLatestAssociationsPlugin)
