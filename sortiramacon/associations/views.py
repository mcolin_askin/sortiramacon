# Create your views here.
from django.http import Http404

from django.views.generic import list_detail

from associations.models import Association
from associations.models import AssociationCategory

def archive_index(request):
    
    queryset = AssociationCategory.objects.all()
    
    
    return list_detail.object_list(
                        request,
                        queryset,
                        paginate_by=50)


def archive_category_index(request, category):
    cat = AssociationCategory.objects.get(slug=category)
    
    queryset = Association.published.filter(category=cat)
    
    
    return list_detail.object_list(
                        request,
                        queryset,
                        paginate_by=15)

def object_detail(request, category, slug):
    queryset = Association.published.all()
    
    return list_detail.object_detail(
                        request,
                        queryset,
                        slug=slug)
