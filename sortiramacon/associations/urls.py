from django.conf.urls.defaults import *

urlpatterns = patterns('associations.views',
    # By category --------
    
    url(
        r'^$', 
        'archive_index', 
        name='association_archive_index'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/$', 
        'archive_category_index', 
        name='association_archive_category_index'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$',
        'object_detail', 
        name='association_detail'
    ),
)

