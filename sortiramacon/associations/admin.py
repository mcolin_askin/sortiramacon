from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext

from associations.forms import AssociationForm, AssociationCategoryForm
from associations.models import Association, AssociationPhoto, AssociationCategory


class AssociationCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'order')
    form = AssociationCategoryForm


class AssociationPhotoAdminInline(admin.TabularInline):
    model = AssociationPhoto


class AssociationAdmin(admin.ModelAdmin):
    """ Admin for events """
    
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('slug', 'title', 'category','is_published',)
    list_filter = ('is_published', 'category',)
    search_fields = ['title', 'content']
    form = AssociationForm
    
    #filter_horizontal = ['news', ]
    
    actions = ['make_published', 'make_unpublished']
    
    save_as = True
    save_on_top = True
    
    inlines = [AssociationPhotoAdminInline,]
    
    def queryset(self, request):
        """ Override to use the objects and not just the default visibles only. """
        return Association.objects.all()
    
    def make_published(self, request, queryset):
        """ Marks selected news items as published """
        rows_updated = queryset.update(is_published=True)
        self.message_user(request, ungettext('%(count)d association was published', 
                                             '%(count)d associations where published', 
                                             rows_updated) % {'count': rows_updated})
    make_published.short_description = _('Publish selected associations')
    
    def make_unpublished(self, request, queryset):
        """ Marks selected news items as unpublished """
        rows_updated =queryset.update(is_published=False)
        self.message_user(request, ungettext('%(count)d association was unpublished', 
                                             '%(count)d associations where unpublished', 
                                             rows_updated) % {'count': rows_updated})
    make_unpublished.short_description = _('Unpublish selected associations')


admin.site.register(Association, AssociationAdmin)
admin.site.register(AssociationCategory, AssociationCategoryAdmin)
