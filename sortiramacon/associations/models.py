# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.conf import settings

from cms.models import CMSPlugin


class AssociationCategory(models.Model):
    """
    Category
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    slug            = models.SlugField(_('Slug'))
    
    content         = models.TextField(_('content'), blank=True)
    
    order           = models.IntegerField(_('order'), default=20)
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ('order', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('association_archive_category_index', (), {
            'category': self.slug,
            })


class PublishedAssociationsManager(models.Manager):
    """ Filters out all unpublished and items with a publication date in the future. """
    
    def get_query_set(self):
        return super(PublishedAssociationsManager, self).get_query_set().filter(is_published=True)


class Association(models.Model):
    """ Association model """
    
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), unique=True, 
                            help_text=_('A slug is a short name which uniquely identifies the news item for this day'))
    
    
    phone   = models.CharField(_('phone'), blank=True, max_length=80)
    address = models.TextField(_('address'), blank=True)
    website = models.CharField(_('website'), blank=True, max_length=250)
    email   = models.CharField(_('email'), blank=True, max_length=80)
    
    content = models.TextField(_('Content'), blank=True)
    
    image  = models.ImageField(_('Image'), upload_to="associations/", null=True, blank=True)
    
    category = models.ForeignKey(AssociationCategory, related_name="a_category", verbose_name=_("Category"))
    
    is_published = models.BooleanField(_('Published'), default=False)
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    
    published = PublishedAssociationsManager()
    objects   = models.Manager()
    
    
    class Meta:
        verbose_name = _('Association')
        verbose_name_plural = _('Associations')
        ordering = ('title', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('association_detail', (), {
            'category': self.category.slug,
            'slug': self.slug,
            })


class LatestAssociationsPlugin(CMSPlugin):
    """ Model for the settings when using the latest news cms plugin. """
    
    category = models.ForeignKey(AssociationCategory, null=True, blank=True, default=None)
    
    limit    = models.PositiveIntegerField(_('Number of items to show'), 
               help_text=_('Limits the number of items that will be displayed'))


class AssociationPhoto(models.Model):
    """
    Photo for association
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    image           = models.ImageField(_('Image'), upload_to="associations/photos/",)
    
    order           = models.IntegerField(_('order'), default=20)
    
    restaurant      = models.ForeignKey(Association, verbose_name=_('association'))
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    
    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Potos')
        ordering = ('order', 'created')
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'

