from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class AssociationApphook(CMSApp):
    name = _("Latest associations")
    urls = ["associations.urls"]

apphook_pool.register(AssociationApphook)
