#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module urls configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from django.conf.urls.defaults import *

from events.models import Event

events_info_dict = {
    'queryset': Event.published.all(),
    'date_field': 'start_date',
}

events_info_month_dict = {
    'queryset': Event.published.all(),
    'date_field': 'start_date',
    'month_format': '%m',
    'allow_future': True
}

urlpatterns = patterns('django.views.generic.date_based',
    (r'^$', 'archive_index', events_info_dict, 'events_archive_index'),
    
    (r'^(?P<year>\d{4})/$',
     'archive_year', events_info_dict, 'events_archive_year'),
    
    #(r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
    # 'archive_month', events_info_month_dict, 'event_archive_month'),
    
    (r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
     'archive_day', events_info_month_dict, 'events_archive_day'),
    
    #(r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
    # 'object_detail', news_info_month_dict, 'news_detail'),
)

urlpatterns += patterns('events.views',
    # By category --------
    
    url(
        r'^(?P<category>[-\w]+)/$', 
        'archive_category_index', 
        name='event_archive_category_index'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/$',
        'archive_category_year', 
        name='event_archive_category_year'
    ),
    
    url(
        r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
        'archive_month', 
        name='event_archive_month'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/$',
        'archive_category_month', 
        name='event_archive_category_month'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
        'archive_category_day', 
        name='event_archive_category_day'
    ),
    
    url(
        r'^(?P<category>[-\w]+)/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        'object_detail', 
        name='event_detail'
    ),
)

