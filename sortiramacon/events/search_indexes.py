#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module search indexes configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from haystack import indexes
from cms_search.search_helpers.indexes import MultiLanguageIndex

from events.models import Event

class EventIndex(indexes.SearchIndex, indexes.Indexable):
    text  = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    url   = indexes.CharField(stored=True, model_attr="slug")
    
    def get_model(self):
        return Event
    
    def prepare_url(self, obj):
        return obj.get_absolute_url()
    
    class HaystackTrans:
        fields = ('url', 'title')