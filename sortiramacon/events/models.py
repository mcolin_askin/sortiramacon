#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module models configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


import datetime
from datetime import date
from datetime import timedelta

from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.conf import settings

from cms.models import CMSPlugin

from news.models import Category, News


class PublishedEventsManager(models.Manager):
    """ Filters out all unpublished and items with a publication date in the future. """
    
    def get_query_set(self):
        return super(PublishedEventsManager, self).get_query_set().filter(is_published=True)


class Event(models.Model):
    """ Event model """
    
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), unique_for_date='start_date', 
                            help_text=_('A slug is a short name which uniquely identifies the news item for this day'))
    
    
    content = models.TextField(_('Content'), blank=True)
    
    image  = models.ImageField(_('Image'), upload_to="news/", null=True, blank=True)
    
    category = models.ForeignKey(Category, related_name="e_category", verbose_name=_("Category"))
    place    = models.ForeignKey("Place", related_name="e_place", verbose_name=_("Place"))
    
    is_published = models.BooleanField(_('Published'), default=False)
    
    start_date   = models.DateTimeField(_('Start date'))
    end_date     = models.DateTimeField(_('End date'))
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    
    news    = models.ManyToManyField(News, verbose_name=_('news'), related_name="news_events", blank=True)
    
    published = PublishedEventsManager()
    objects   = models.Manager()
    
    
    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')
        ordering = ('-start_date', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('event_detail', (), {
            'category': self.category.slug,
            'year': self.start_date.strftime("%Y"),
            'month': self.start_date.strftime("%m"),
            'day': self.start_date.strftime("%d"),
            'slug': self.slug,
            })
    
    def get_next(self):
        try:
            return Event.published.filter(start_date__gt=self.start_date)[0]
        except:
            return None
    
    def get_previous(self):
        try:
            return Event.published.filter(start_date__lt=self.start_date).order_by('-start_date')[0]
        except:
            return None


class Place(models.Model):
    """
    Place
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    slug            = models.SlugField(_('Slug'))
    
    address         = models.TextField(_('address'), blank=True)
    email           = models.EmailField(_('email'), blank=True)
    website         = models.URLField(_('website'), blank=True, verify_exists=False)
    phone           = models.CharField(_('phone'), blank=True, max_length=80)
    
    image           = models.ImageField(_('Image'), 
                                    upload_to="places/", null=True, blank=True)
    
    content         = models.TextField(_('content'), blank=True)
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    
    class Meta:
        verbose_name = _('Place')
        verbose_name_plural = _('Places')
        ordering = ('title', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        return ('place_detail', (), {
            'slug': self.slug,
            })


    def get_current_showtimes(self):
        """Return the current showtimes for this movie"""
        return self.showtime_place.all().order('movie', 'start_date')
        current_date = date.today()
        return self.showtime_place.filter(start_date__lte=current_date, 
                                          end_date__gte=current_date)

    def get_this_week_showtimes(self):
        """Return this week showtimes for this movie"""
        
        current_date = date.today()

        # if we are wednesday or more, show the current week, else, show before
        if current_date.isoweekday() >= 3:
            start_date = current_date - timedelta(
                days=abs(3-current_date.isoweekday()))
        else:
            start_date = current_date - timedelta(
                days=abs(4+current_date.isoweekday()))
        end_date = start_date + timedelta(days=7)
        
        try:
            showtimes = self.showtime_place.filter(
                start_date__gte=start_date, 
                start_date__lte=end_date).order_by('movie', 'start_date')
        except Exception:
            return []


        # start_date = wednesday
        # end_date = next tuesday

        this_week = []
        movies = []
        for i in showtimes:
            if i.movie not in movies:
                movies.append(i.movie)

        for movie in movies:
            movie_dates = []
            tmp_date = start_date
            while tmp_date < end_date:
                date_added = False
                for i in showtimes:
                    if i.movie == movie and i.start_date == tmp_date:
                        date_added = True
                        movie_dates.append({
                            "date": tmp_date,
                            "showtime": i.content
                        })
                        break

                if date_added == False:
                    movie_dates.append({
                        "date": tmp_date,
                        "showtime": u"Pas de séance"
                        })

                tmp_date += timedelta(days=1)
            this_week.append({
                "movie": movie,
                "dates": movie_dates
            })

        return {
            "start_date": start_date,
            "end_date": end_date,
            "movies": this_week
        }


class LatestEventPlugin(CMSPlugin):
    """ Model for the settings when using the latest news cms plugin. """
    
    category = models.ForeignKey(Category, null=True, blank=True, default=None)
    place    = models.ForeignKey(Place, null=True, blank=True, default=None)
    
    limit    = models.PositiveIntegerField(_('Number of items to show'), 
               help_text=_('Limits the number of items that will be displayed'))


class EventPhoto(models.Model):
    """
    Photo for event
    """
    
    title           = models.CharField(_('Title'), max_length=255)
    
    image           = models.ImageField(_('Image'), upload_to="events/",)
    
    order           = models.IntegerField(_('order'), default=20)
    
    event           = models.ForeignKey(Event, verbose_name=_('event'))
    
    created         = models.DateTimeField(auto_now_add=True, editable=False)
    updated         = models.DateTimeField(auto_now=True, editable=False)
    
    
    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Potos')
        ordering = ('order', 'created')
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.title)
        except:
            return u'Auncun titre'

