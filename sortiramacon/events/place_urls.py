#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module urls configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'

from django.conf.urls.defaults import *

from events.models import Place

places_info_dict = {
    'queryset': Place.objects.all(),
}

urlpatterns = patterns('django.views.generic.list_detail',
    (r'^$', 'object_list', places_info_dict, 'places_index'),
    
    # url(
    #     r'^(?P<slug>[-\w]+)/$',
    #     'object_detail', 
    #     { 
    #         'slug_field': 'slug',
    #         'queryset': Place.objects.all()
    #     },
    #     name='place_detail'
    # ),
)

urlpatterns += patterns('events.views',
    
    url(
        r'^(?P<slug>[-\w]+)/$',
        'place_detail', 
        
        name='place_detail'
    ),
)


