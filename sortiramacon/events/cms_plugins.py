#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module cms plugins configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from events.models import LatestEventsPlugin, Events
from events import settings

from django.conf import settings

class CMSLatestEvetnsPlugin(CMSPluginBase):
    """ Plugin class for the latest news """
    model = LatestEventsPlugin

    name = _('Latest events')
    render_template = 'events/latest_events.html'
    admin_preview = False
    
    def render(self, context, instance, placeholder, extra=None):
        """ Render the latest news """
        query = Event.published
        
        if instance.category is None:
            latest = query.all()[:instance.limit]
        else:
            latest = query.filter(category=instance.category).all()[:instance.limit]
        
        if instance.template is not None and instance.template != "":
            self.render_template = instance.template
        
        context.update({
            'instance': instance,
            'latest': latest,
            'placeholder': placeholder,
        })
        return context

plugin_pool.register_plugin(CMSLatestEventsPlugin)
