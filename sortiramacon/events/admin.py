#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module admin configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext

from events.forms import EventForm
from events.forms import PlaceForm
from events.models import Event
from events.models import Place
from events.models import EventPhoto


class EventPhotoAdminInline(admin.TabularInline):
    model = EventPhoto


class EventAdmin(admin.ModelAdmin):
    """ Admin for events """
    
    date_hierarchy = 'start_date'
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('slug', 'title', 'category','is_published', 'start_date', 'end_date')
    list_filter = ('is_published', 'category',)
    search_fields = ['title', 'content']
    form = EventForm
    
    filter_horizontal = ['news', ]
    
    actions = ['make_published', 'make_unpublished']
    
    save_as = True
    save_on_top = True
    
    inlines = [EventPhotoAdminInline,]
    
    def queryset(self, request):
        """ Override to use the objects and not just the default visibles only. """
        return Event.objects.all()
    
    def make_published(self, request, queryset):
        """ Marks selected news items as published """
        rows_updated = queryset.update(is_published=True)
        self.message_user(request, ungettext('%(count)d event was published', 
                                             '%(count)d events where published', 
                                             rows_updated) % {'count': rows_updated})
    make_published.short_description = _('Publish selected news')
    
    def make_unpublished(self, request, queryset):
        """ Marks selected news items as unpublished """
        rows_updated =queryset.update(is_published=False)
        self.message_user(request, ungettext('%(count)d event was unpublished', 
                                             '%(count)d events where unpublished', 
                                             rows_updated) % {'count': rows_updated})
    make_unpublished.short_description = _('Unpublish selected news')


class PlaceAdmin(admin.ModelAdmin):
    """ Admin for places """
    
    list_display = ('slug', 'title', 'address')
    search_fields = ['title', 'content']
    prepopulated_fields = {"slug": ("title",)}
    form = PlaceForm
    
    actions = []
    
    save_as = True
    save_on_top = True
    
    def queryset(self, request):
        """ Override to use the objects and not just the default visibles only. """
        return Place.objects.all()


admin.site.register(Event, EventAdmin)
admin.site.register(Place, PlaceAdmin)
