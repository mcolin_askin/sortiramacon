from django.contrib.sitemaps import Sitemap
from .models import Event

class EventsSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Event.published.all()

    def lastmod(self, obj):
        return obj.updated