#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Events module views configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'

import calendar
import time
from datetime import date, datetime, timedelta

from django.http import Http404

from django.shortcuts import render_to_response
from django.views.generic import date_based
from django.views.generic import list_detail

from events.models import Event
from events.models import Place
from news.models import Category

mnames = u"Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre"
mnames = mnames.split()


def archive_category_index(request, category):
    cat = Category.objects.get(slug=category)
    
    queryset = Event.published.filter(category=cat)
    
    date_field = "start_date"
    
    return list_detail.object_list(
                        request,
                        queryset,
                        paginate_by=10,
                        extra_context={"category": cat})

def archive_category_year(request, category, year):
    try:
        cat = Category.objects.get(slug=category)
        
        queryset = Event.published.filter(category=cat)
        
        date_field = "start_date"
        
        return date_based.archive_year(
                            request,
                            year,
                            queryset,
                            date_field,
                            allow_future=True)
    except:
        raise Http404

def archive_month(request, year, month):
    """Listing of days in `month`."""
    year, month = int(year), int(month)
    
    # init variables
    cal = calendar.Calendar()
    
    month_days          = cal.itermonthdays(year, month)
    nyear, nmonth, nday = time.localtime()[:3]
    
    lst = [[]]
    week = 0
    
    # make month lists containing list of days for each week
    # each day tuple will contain list of entries and 'current' indicator
    for day in month_days:
        entries = current = False   # are there entries for this day; current day?
        if day:
            entries = Event.objects.filter(start_date__year=year, start_date__month=month, start_date__day=day)
            if day == nday and year == nyear and month == nmonth:
                current = True
        day_n = day
        if day_n < 10:
            day_n = "0%d" % day_n
        lst[week].append((day, entries, current, day_n))
        if len(lst[week]) == 7:
            lst.append([])
            week += 1
    
    month_number=month
    if month < 10:
        month_number = "0%d" % month
    else:
        month_number = "%d" % month
    
    previous_month = month - 1
    previous_year = year
    if previous_month == 0:
        previous_month = 12
        previous_year  -= 1
    elif previous_month == 13:
        previous_month = 1
        previous_year += 1
    
    if previous_month < 10:
        previous_month = "0%d" % previous_month
    else:
        previous_month = "%d" % previous_month
    
    next_month = month + 1
    next_year = year
    if next_month == 0:
        next_month = 12
        next_year  -= 1
    elif next_month == 13:
        next_month = 1
        next_year += 1
    
    if next_month < 10:
        next_month = "0%d" % next_month
    else:
        next_month = "%d" % next_month
    
    return render_to_response("events/event_archive_month.html", {
                                                    "year":  year, 
                                                    "month": month,
                                                    "month_days": lst, 
                                                    "mname": mnames[month-1], 
                                                    "month_n": month_number,
                                                    "p_month": previous_month,
                                                    "p_year": previous_year,
                                                    "n_month": next_month,
                                                    "n_year": next_year})
                                                    
def archive_category_month(request, category, year, month):
    """Listing of days in `month`."""
    cat = Category.objects.get(slug=category)
    year, month = int(year), int(month)
    
    # init variables
    cal = calendar.Calendar()
    
    month_days          = cal.itermonthdays(year, month)
    nyear, nmonth, nday = time.localtime()[:3]
    
    lst = [[]]
    week = 0
    
    # make month lists containing list of days for each week
    # each day tuple will contain list of entries and 'current' indicator
    for day in month_days:
        entries = current = False   # are there entries for this day; current day?
        if day:
            entries = Event.objects.filter(category=cat, start_date__year=year, start_date__month=month, start_date__day=day)
            if day == nday and year == nyear and month == nmonth:
                current = True

        lst[week].append((day, entries, current))
        if len(lst[week]) == 7:
            lst.append([])
            week += 1
    month_number=month
    if month < 10:
        month_number = "0%d" % month
    else:
        month_number = "%d" % month
    
    previous_month = month - 1
    previous_year = year
    if previous_month == 0:
        previous_month = 12
        previous_year  -= 1
    elif previous_month == 13:
        previous_month = 1
        previous_year += 1
    
    if previous_month < 10:
        previous_month = "0%d" % previous_month
    else:
        previous_month = "%d" % previous_month
    
    next_month = month + 1
    next_year = year
    if next_month == 0:
        next_month = 12
        next_year  -= 1
    elif next_month == 13:
        next_month = 1
        next_year += 1
    
    if next_month < 10:
        next_month = "0%d" % next_month
    else:
        next_month = "%d" % next_month
    
    return render_to_response("events/event_category_archive_month.html", {
                                                    "year":  year, 
                                                    "month": month,
                                                    "month_days": lst, 
                                                    "mname": mnames[month-1], 
                                                    "category": cat,
                                                    "month_n": month_number,
                                                    "p_month": previous_month,
                                                    "p_year": previous_year,
                                                    "n_month": next_month,
                                                    "n_year": next_year})


def archive_category_day(request, category, year, month, day):
    try:
        cat = Category.objects.get(slug=category)
        
        queryset = Event.published.filter(category=cat)
        
        date_field = "start_date"
        
        return date_based.archive_day(
                            request,
                            year,
                            month,
                            day,
                            queryset,
                            date_field,
                            '%m',
                            allow_future=True)
    except:
        raise Http404

def object_detail(request, category, year, month, day, slug):
    try:
        cat = Category.objects.get(slug=category)
        queryset = Event.published.filter(category=cat)
    except:
        queryset = Event.published.all()
    date_field = "start_date"
    
    return date_based.object_detail(
                        request,
                        year,
                        month,
                        day,
                        queryset,
                        date_field,
                        '%m',
                        slug=slug,
                        allow_future=True)


def place_detail(request, slug):
    queryset = Place.objects.all()
    try:
        place = queryset.filter(slug=slug)[0]
        extra_context = {'showtimes': place.get_this_week_showtimes() }
    except:
        raise
        extra_context = {}
    print extra_context
    return list_detail.object_detail(
                        request,
                        queryset,
                        slug=slug,
                        extra_context=extra_context)
