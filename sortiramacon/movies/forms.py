#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Movies module forms configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from django import forms
from django.conf import settings

from cms.plugin_pool import plugin_pool
from cms.plugins.text.settings import USE_TINYMCE
from cms.plugins.text.widgets.wymeditor_widget import WYMEditor

from movies.models import Movie


class MovieForm(forms.ModelForm):
    """Form definition for the movie model

    The form definition allow the movie model to have the WYSIWG editor with 
    all the zebnetCMS functionnality (plugins).
    """
    
    
    class Meta:
        """Meta model configuration"""
        model = Movie
    

    def __init__(self, *args, **kwargs):
        """Initialisation method for the model"""
        super(MovieForm, self).__init__(*args, **kwargs)
        widget = self._get_widget()
        self.fields['content'].widget = widget

    def _get_widget(self):
        """Return activated widget for the editor"""
        plugins = plugin_pool.get_text_enabled_plugins(placeholder=None, 
                                                       page=None)
        if USE_TINYMCE and "tinymce" in settings.INSTALLED_APPS:
            from news.widgets.tinymce_widget import TinyMCEEditor
            return TinyMCEEditor(installed_plugins=plugins)
        return WYMEditor(installed_plugins=plugins)

