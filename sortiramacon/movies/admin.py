#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Admin configuration for the movies module"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from django.contrib import admin

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext

from movies.forms import MovieForm

from movies.models import Movie
from movies.models import Showtime


class ShowtimeAdminInline(admin.TabularInline):
    """Inline admin part for showtime editing in a movie admin part"""
    model = Showtime


class MovieAdmin(admin.ModelAdmin):
    """Admin configuration for movie model"""
    
    date_hierarchy = 'release_date'
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('slug', 'title', 'is_published', 'release_date',)
    list_filter = ('is_published',)
    search_fields = ['title', 'content']

    form = MovieForm
    
    actions = ['make_published', 'make_unpublished']
    
    save_as = True
    save_on_top = True
    
    inlines = [ShowtimeAdminInline,]
    
    def queryset(self, request):
        """Override to use the objects and not just the default visibles."""
        return Movie.objects.all()
    
    def make_published(self, request, queryset):
        """Marks selected movie items as published"""
        rows_updated = queryset.update(is_published=True)
        self.message_user(request, ungettext(
            '%(count)d movie was published', 
            '%(count)d movies where published', 
            rows_updated) % {'count': rows_updated})
    make_published.short_description = _('Publish selected movies')
    
    def make_unpublished(self, request, queryset):
        """Marks selected movie items as unpublished"""
        rows_updated =queryset.update(is_published=False)
        self.message_user(request, ungettext(
            '%(count)d movie was unpublished', 
            '%(count)d movies where unpublished', 
            rows_updated) % {'count': rows_updated})
    make_unpublished.short_description = _('Unpublish selected movies')

admin.site.register(Movie, MovieAdmin)
