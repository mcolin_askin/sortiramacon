#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Movies module custom views"""

__author__ = 'AskinWeb <contact@askinweb.fr>'

from datetime import date
from datetime import timedelta

from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views.generic import date_based

from .models import Movie


def index(request):
    current_date = date.today()

    # Getting the previous wednesday
    while current_date.isoweekday() != 3:
        current_date -= timedelta(days=1)

    return redirect('movie_archive_day', 
                    year=current_date.strftime("%Y"),
                    month=current_date.strftime("%m"),
                    day=current_date.strftime("%d"))



def object_detail(request, year, month, day, slug):
    queryset = Movie.published.all()
    date_field = 'release_date'
    try:
        movie = queryset.filter(slug=slug)[0]
        extra_context = {'showtimes': movie.get_this_week_showtimes() }
    except:
        extra_context = {}
    return date_based.object_detail(
                        request,
                        year,
                        month,
                        day,
                        queryset,
                        date_field,
                        month_format='%m',
                        slug=slug,
                        allow_future=True,
                        extra_context=extra_context)
