#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Movies module urls configuration"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from django.conf.urls.defaults import *

from movies.models import Movie


MOVIES_YEAR_INFO_DICT = {
    'queryset': Movie.published.all(),
    'date_field': 'release_date',
    'allow_future': True,
}

MOVIES_INFO_DICT = {
    'queryset': Movie.published.all(),
    'date_field': 'release_date',
    'allow_future': True,
    'month_format': '%m',
}

urlpatterns = patterns('django.views.generic.date_based',
    (
        r'^(?P<year>\d{4})/$',
        'archive_year', 
        MOVIES_YEAR_INFO_DICT, 
        'movie_archive_year'
    ),
    
    (
        r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
        'archive_month', 
        MOVIES_INFO_DICT, 
        'movie_archive_month'
    ),
    
    (
        r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/$',
        'archive_day', 
        MOVIES_INFO_DICT, 
        'movie_archive_day'
    ),

    # (
    #     r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
    #     'object_detail',
    #     MOVIES_INFO_DICT,
    #     'movie_detail'
    # ),
    
)


urlpatterns += patterns('movies.views',
    (
        r'^$', 
        'index', 
    ),
    url(
        r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
        'object_detail', 
        name='movie_detail'
    ),
)