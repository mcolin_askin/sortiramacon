from django.contrib.sitemaps import Sitemap
from .models import Movie

class MoviesSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Movie.published.all()

    def lastmod(self, obj):
        return obj.release_date