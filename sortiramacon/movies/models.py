#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Admin configuration for the movies module"""

__author__ = 'AskinWeb <contact@askinweb.fr>'


from datetime import date
from datetime import timedelta

from django.db import models
from django.utils.translation import ugettext_lazy as _

from events.models import Place



class PublishedMoviesManager(models.Manager):
    """Filters out all unpublished and items with a status to published."""
    
    def get_query_set(self):
        return super(PublishedMoviesManager, self).get_query_set().filter(
            is_published=True)


class Movie(models.Model):
    """Movie model definition"""
    
    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'), unique_for_date='release_date',
        help_text=_('A slug is a short name which uniquely identifies a movie'))
    
    
    release_date = models.DateField(_('Date de sortie'))

    content = models.TextField(_('Content'), blank=True)
    
    image = models.ImageField(_('Image'), upload_to="affiche/", null=True)
    
    is_published = models.BooleanField(_('Published'), default=False)
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    
    published = PublishedMoviesManager()
    objects = models.Manager()
    
    
    class Meta:
        """Meta configuration for the model"""
        verbose_name = _('movie')
        verbose_name_plural = _('movies')
        ordering = ('-release_date', )
    
    
    def __unicode__(self):
        """Default rendering method"""
        try:
            return u'%s' % (self.title)
        except Exception:
            return u'Auncun titre'
    
    @models.permalink
    def get_absolute_url(self):
        """Return the absolute url for this item"""
        return ('movie_detail', (), {
            'year': self.release_date.strftime("%Y"),
            'month': self.release_date.strftime("%m"),
            'day': self.release_date.strftime("%d"),
            'slug': self.slug,
        })

    def get_current_showtimes(self):
        """Return the current showtimes for this movie"""
        #return self.showtime_movie.all().order_by('place', 'start_date')

        current_date = date.today()

        # if we are wednesday or more, show the current week, else, show before
        if current_date.isoweekday() >= 3:
            start_date = current_date - timedelta(days=current_date.isoweekday() - 1)
        else:
            start_date = current_date - timedelta(days=current_date.isoweekday() - 8)
        end_date = start_date + timedelta(days=7)
        print start_date, end_date
        try:
            return self.showtime_movie.filter(
                start_date__gte=start_date, 
                start_date__lte=end_date).order_by('place', 'start_date')
        except Exception:
            return None

    def get_this_week_showtimes(self):
        """Return this week showtimes for this movie"""
        
        current_date = date.today()

        # if we are wednesday or more, show the current week, else, show before
        if current_date.isoweekday() >= 3:
            start_date = current_date - timedelta(
                days=abs(3-current_date.isoweekday()))
        else:
            start_date = current_date - timedelta(
                days=abs(4+current_date.isoweekday()))
        end_date = start_date + timedelta(days=7)
        
        try:
            showtimes = self.showtime_movie.filter(
                start_date__gte=start_date, 
                start_date__lte=end_date).order_by('place', 'start_date')
        except Exception:
            return []


        # start_date = wednesday
        # end_date = next tuesday

        this_week = []
        places = []
        for i in showtimes:
            if i.place not in places:
                places.append(i.place)

        for place in places:
            place_dates = []
            tmp_date = start_date
            while tmp_date < end_date:
                date_added = False
                for i in showtimes:
                    if i.place == place and i.start_date == tmp_date:
                        date_added = True
                        place_dates.append({
                            "date": tmp_date,
                            "showtime": i.content
                        })
                        break

                if date_added == False:
                    place_dates.append({
                        "date": tmp_date,
                        "showtime": u"Pas de séance"
                        })

                tmp_date += timedelta(days=1)
            this_week.append({
                "place": place,
                "dates": place_dates
            })

        return {
            "start_date": start_date,
            "end_date": end_date,
            "places": this_week
        }
            


    def get_current_showtimes_for_place(self, place):
        """Return the current showtime for this movie and the place"""
        current_date = date.today()

        try:
            return self.showtime_movie.filter(start_date__lte=current_date, 
                                              start_date__gte=current_date,
                                              place=place)[0]
        except Exception:
            return None


class Showtime(models.Model):
    """Showtime model definition

    A showtime allow to indicate, for a movie and a place, the different
    projection of a movie in the place
    """
    movie = models.ForeignKey(Movie, verbose_name=_(u"Film"), 
                              related_name="showtime_movie")
    place = models.ForeignKey(Place, verbose_name=_(u"Lieu"), 
                              related_name="showtime_place")
    start_date = models.DateField(_("start date"))
    end_date = models.DateField(_("end date"), auto_now_add=True)

    content = models.TextField(_('showtimes'))
    

    class Meta:
        ordering = ["start_date"]
        verbose_name = _("showtime")
        verbose_name_plural = _("showtimes")

