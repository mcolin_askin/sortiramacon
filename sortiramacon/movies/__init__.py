#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (c) 2012 ASK interactive. All Rights Reserved.
#
# Created by AskinWeb <contact@askinweb.fr> on 2012-03-08.

"""Movies module include elements for movies and showtime part of the website"""

__author__ = 'AskinWeb <contact@askinweb.fr>'