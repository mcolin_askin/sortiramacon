# -*- coding: utf-8 -*-


from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponse, Http404, HttpResponseForbidden, HttpResponseBadRequest
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils import simplejson
from django.core import serializers

from django.template.context import RequestContext
from django.conf import settings
from django.template.defaultfilters import escapejs, force_escape
from django.views.decorators.http import require_POST

from cms.models import Page, Title, CMSPlugin, MASK_CHILDREN, MASK_DESCENDANTS,\
    MASK_PAGE
from cms.plugin_pool import plugin_pool
from cms.utils.admin import render_admin_menu_item
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from cms.utils import get_language_from_request
from django.http import Http404, HttpResponse

def crossdomain(request):
    return HttpResponse(
"""
    <?xml version="1.0"?>
<!DOCTYPE cross-domain-policy 
  SYSTEM "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">
<cross-domain-policy>
        <allow-access-from domain="*" />
</cross-domain-policy>""")

def plugin_set_attr(request, plugin_id, attr_name, attr_value):
    #if not page.has_change_permission(request):
    #    raise Http404
    plugin = CMSPlugin.objects.get(pk=plugin_id)
    try:
        instance, admin = plugin.get_plugin_instance()
        if instance:
            setattr(instance, attr_name, attr_value)
            instance.save()
        else:
            setattr(plugin, attr_name, attr_value)
            plugin.save()
    except:
        pass
    return HttpResponse()


def plugin_get(request, plugin_id):
    #if not page.has_change_permission(request):
    #    raise Http404
    plugin = CMSPlugin.objects.get(pk=plugin_id)
    try:
        instance, admin = plugin.get_plugin_instance()
        if instance:
            obj = instance
        else:
            obj = plugin
    except:
        obj = None
    data = serializers.serialize('json', [obj])
    return HttpResponse(simplejson.dumps(simplejson.loads(data)),
                        content_type='application/json')

