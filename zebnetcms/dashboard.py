# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard

from datetime import date, timedelta

from django.conf import settings

class ZebnetCMSIndexDashboard(Dashboard):
    """
    Custom index dashboard for pact.
    """
    title = False
    stats = []
    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)
        
        
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            title=_('Gestion du contenu'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            children=(
                {
                    'title': _('Ajouter un page'),
                    'url': reverse('admin:cms_page_add'),
                    'external': False,
                    'description': u'Ajouter une page à votre site',
                    'css_classes': ('linkimg', 'add_page')
                },
                {
                    'title': _('Plan du site'),
                    'url': reverse('admin:cms_page_changelist'),
                    'external': False,
                    'css_classes': ('linkimg', 'sitemap')
                },
                {
                    'title': _(u'Édition sur site'),
                    'url': '/?preview',
                    'external': False,
                    'description': u'Éditer vos textes directement sur votre site',
                    'css_classes': ('linkimg', 'front_edit')

                },
                #{
                #    'title': _(u'Bibliothèque'),
                #    'url': '/admin/image_filer/folder/',
                #    'external': False,
                #    'description': u'Gérer votrte bibliothèque d\'images',
                #    'css_classes': ('linkimg', 'image_folder')
                #},
            )
        ))

        # append an app list module for "Applications"
        #self.children.append(modules.AppList(
        #    title=_('Applications'),
        #    exclude_list=('django.contrib',),
        #))

        # append an app list module for "Administration"
        #self.children.append(modules.AppList(
        #    title=_('Administration'),
        #    include_list=('django.contrib',),
        #))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            title=_('Recent Actions'),
            limit=5
        ))

        # append a feed module
        #self.children.append(modules.Feed(
        #    title=_(u'L\'Actualité de zebNet'),
        #    feed_url='http://www.zebnet.fr/rss/',
        #    limit=5
        #))

        # append another link list module for "support".
        #self.children.append(modules.LinkList(
        #    title=_('Support'),
        #    children=[
        #        {
        #            'title': _('Manuel d\'utilisateur'),
        #            'url': 'http://docs.djangoproject.com/',
        #            'external': True,
        #        },
        #    ]
        #))
        
        from zebnetcms.models import AnalyticsStat
        
        end_date = date.today() - timedelta(days=1)
        start_date = end_date - timedelta(days=30)
        
        stats = AnalyticsStat.objects.filter(date__lte=end_date, 
                                             date__gte=start_date)
        self.stats = stats
        print self.stats

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        pass
        
        
        


# to activate your app index dashboard add the following to your settings.py:
#
# ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'pact.dashboard.CustomAppIndexDashboard'

class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for pact.
    """
    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # we disable title because its redundant with the model list module
        self.title = ''

        # append a model list module
        self.children.append(modules.ModelList(
            title=self.app_title,
            include_list=self.models,
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            title=_('Recent Actions'),
            include_list=self.get_app_content_types(),
        ))

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        pass
