# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

from cms.sitemaps import CMSSitemap

urlpatterns = patterns('',
    (r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
    
    #(r'^media/(?P<path>.*)$', 'django.views.static.serve',
    #    {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
    
    (r'^crossdomain.xml$', 'zebnetcms.views.crossdomain'),
    
    (r'^forms/', include('form_designer.urls')),
    
    url(r'^admin_tools/', include('admin_tools.urls')),
    
    url(r'^admin/plugin/(?P<plugin_id>[^/]+)/get/', 
        'zebnetcms.views.plugin_get', name='admin_plugin_get'),
    
    url(r'^admin/plugin/(?P<plugin_id>[^/]+)/set/(?P<attr_name>[^/]+)/(?P<attr_value>[^/]+)/', 
        'zebnetcms.views.plugin_set_attr', name='admin_plugin_set_attr'),
    
    (r'^admin/form_designer/', include('form_designer.admin_urls')),
    
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
    
    url(r'^', include('cms.urls')),
)
