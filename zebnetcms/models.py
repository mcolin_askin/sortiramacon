# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.conf import settings

class AnalyticsStat(models.Model):
    """ Analytics stat model """
    
    date = models.DateField(_('date'))
    pageviews = models.IntegerField(_('pageviews'))
    visitors = models.IntegerField(_('visitors'))
    
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    
    
    class Meta:
        verbose_name = _('Analytics Stat')
        verbose_name_plural = _('Analytics Stats')
        ordering = ('date', )
    
    
    def __unicode__(self):
        try:
            return u'%s'% (self.date)
        except:
            return u'Auncune date'

