# -*- coding: utf-8 -*-

from django.contrib import admin

from cms.plugins.text.models import Text
from django.conf import settings

from django.utils.safestring import mark_safe

from cms.admin.change_list import CMSChangeList
from cms.admin.dialog.views import get_copy_dialog
from cms.admin.forms import PageForm, PageAddForm
from cms.admin.permissionadmin import PAGE_ADMIN_INLINES, \
    PagePermissionInlineAdmin
from cms.admin.views import revert_plugins
from cms.apphook_pool import apphook_pool
from cms.exceptions import NoPermissionsException
from cms.forms.widgets import PluginEditor
from cms.models import Page, Title, CMSPlugin, PagePermission, \
    PageModeratorState, EmptyTitle, GlobalPagePermission
from cms.models.managers import PagePermissionsPermissionManager
from cms.models.moderatormodels import MASK_PAGE, MASK_CHILDREN, \
    MASK_DESCENDANTS
from cms.models.placeholdermodel import Placeholder
from cms.plugin_pool import plugin_pool
from cms.utils import get_template_from_request, get_language_from_request
from cms.utils.admin import render_admin_menu_item
from cms.utils.copy_plugins import copy_plugins_to
from cms.utils.helpers import make_revision_with_plugins
from cms.utils.moderator import update_moderation_message, \
    get_test_moderation_level, moderator_should_approve, approve_page, \
    will_require_moderation
from cms.utils.permissions import has_page_add_permission, \
    has_page_change_permission, get_user_permission_level, \
    has_global_change_permissions_permission
from cms.utils.placeholder import get_page_from_placeholder_if_exists
from cms.utils.plugins import get_placeholders, get_page_from_plugin_or_404
from copy import deepcopy
from django import template
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.options import IncorrectLookupParameters
from django.contrib.admin.util import unquote, get_deleted_objects
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import transaction, models
from django.forms import Widget, Textarea, CharField
from django.http import HttpResponseRedirect, HttpResponse, Http404, \
    HttpResponseBadRequest, HttpResponseForbidden, HttpResponseNotAllowed
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.template.defaultfilters import title, escape, force_escape, escapejs
from django.utils.encoding import force_unicode
from django.utils.translation import ugettext_lazy as _

from django.views.decorators.csrf import csrf_exempt


from cms.admin import pageadmin as cms_admin

model_admin = admin.ModelAdmin
create_on_success = lambda x: x

if 'reversion' in settings.INSTALLED_APPS:
    import reversion
    from reversion.admin import VersionAdmin
    model_admin = VersionAdmin
    
    create_on_success = reversion.revision.create_on_success

class PageAdmin(cms_admin.PageAdmin):
    def get_urls(self):
        """Get the admin urls
        """
        from django.conf.urls.defaults import patterns, url
        info = "%s_%s" % (self.model._meta.app_label, self.model._meta.module_name)
        pat = lambda regex, fn: url(regex, self.admin_site.admin_view(fn), name='%s_%s' % (info, fn.__name__))
        
        url_patterns = patterns('',
            pat(r'add-plugin-in-editor/$', self.add_plugin_in_editor),
            pat(r'(?P<object_id>\d+)/edit-plugin/([a-zA-Z]+)/$', self.frontedit_view),
            
        )
        
        url_patterns = url_patterns + super(PageAdmin, self).get_urls()
        return url_patterns
    
    def frontedit_view(self, request, object_id, extra_context=None):
        """
        The 'change' admin view for the Page model.
        """
        try:
            obj = self.model.objects.get(pk=object_id)
        except self.model.DoesNotExist:
            # Don't raise Http404 just yet, because we haven't checked
            # permissions yet. We don't want an unauthenticated user to be able
            # to determine whether a given object exists.
            obj = None
        from django.forms import ModelForm
        from cms.plugins.text.widgets.wymeditor_widget import WYMEditor
        
        # Create the form class.
        class PageForm(ModelForm):
            #content    = forms.CharField(label=_('Content'), required=False, widget=WYMEditor)
            
            class Meta:
                fields = ('id', 'content')
                widgets = {
                    'content': WYMEditor,
                }
                model = Page
        
        
        if request.method == 'POST': # If the form has been submitted...
            form = PageForm(request.POST) # A form bound to the POST data
            if form.is_valid(): # All validation rules pass
                # Process the data in form.cleaned_data
                # ...
                obj.content = form.cleaned_data['content']
                obj.save()
                context = {
                    'CMS_MEDIA_URL': settings.CMS_MEDIA_URL,
                    'plugin': obj,
                    'is_popup': True,
                    'name': unicode(obj),
                    "type": "Text",
                }
                return render_to_response('admin/cms/page/plugin_forms_ok.html', context, RequestContext(request))
        
        form = PageForm(instance=obj)
        media = mark_safe(self.media + form.media)
        opts = self.model._meta
        app_label = opts.app_label
        ordered_objects = opts.get_ordered_objects()
        context = {}
        context.update({
            'form': form,
            'opts': opts,
            'media': media,
            'is_popup': True
            #'content_type_id': ContentType.objects.get_for_model(self.model).id,
        })
        
        form_template = None
        context_instance = template.RequestContext(request, current_app=self.admin_site.name)
        return render_to_response(form_template or [
            "admin/%s/%s/frontedit_form.html" % (app_label, opts.object_name.lower()),
            "admin/%s/frontedit_form.html" % app_label,
            "admin/frontedit_form.html"
        ], context, context_instance=context_instance)
        
    
    def add_plugin_in_editor(self, request):
        '''
        Could be either a page or a parent - if it's a parent we get the page via parent.
        '''
        if 'history' in request.path or 'recover' in request.path:
            return HttpResponse(str("error"))
        if request.method == "POST":
            plugin_type = request.POST['plugin_type']
            #page_id = request.POST.get('page_id', None)
            
            #page = get_object_or_404(Page, pk=page_id)
            
            language = "fr" #request.POST['language'] or get_language_from_request(request)
            # fr
            
            #if not page.has_change_permission(request):
            #    # we raise a 404 instead of 403 for a slightly improved security
            #    # and to be consistent with placeholder admin
            #    raise Http404
            # Sanity check to make sure we're not getting bogus values from JavaScript:
            if not language or not language in [ l[0] for l in settings.LANGUAGES ]:
                return HttpResponseBadRequest(unicode(_("Language must be set to a supported language!")))

            plugin = CMSPlugin(language=language, plugin_type=plugin_type)
            plugin.save()
            #if 'reversion' in settings.INSTALLED_APPS and page:
            #    make_revision_with_plugins(page)
            #    reversion.revision.user = request.user
            #    plugin_name = unicode(plugin_pool.get_plugin(plugin_type).name)
            #    reversion.revision.comment = unicode(_(u"%(plugin_name)s plugin added to %(placeholder)s") % {'plugin_name':plugin_name, 'placeholder':placeholder})
                
            return HttpResponse(str(plugin.pk))
        raise Http404
    
    
admin.site.unregister(Page)
admin.site.register(Page, PageAdmin)

admin.site.register(Text)
