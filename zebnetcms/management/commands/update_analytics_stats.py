from django.core.management.base import BaseCommand, CommandError

from datetime import date, timedelta, datetime

from googleanalytics import Connection

from zebnetcms.models import AnalyticsStat

from django.conf import settings

class Command(BaseCommand):
    args = '[start_date [end_date'
    help = 'Update analytics stats for the specific periode'

    def handle(self, *args, **options):
        try:
            end_date = datetime.strptime(args[1], "%d/%m/%Y")
        except:
            end_date   = date.today() - timedelta(days=1)
        
        try:
            start_date = datetime.strptime(args[0], "%d/%m/%Y")
        except:
            start_date = end_date
        self.stdout.write('Trying to update Anaytics for period %s to %s\n' % (start_date, end_date))
        
        
        
        connection = Connection(settings.GOOGLE_ANALYTICS_API_LOGIN, 
                                settings.GOOGLE_ANALYTICS_API_PASSWORD)
        
        accounts = connection.get_accounts()
        
        account = None
        
        for a in accounts:
            if a.title == settings.GOOGLE_ANALYTICS_API_ACCOUNT:
                account = a
        
        current_date = start_date
        while current_date <= end_date:
            data = account.get_data(start_date=current_date, end_date=current_date, metrics=['pageviews', 'visitors'])
            try:
                d = data[0]
                obj, created = AnalyticsStat.objects.get_or_create(
                                                    date=current_date,
                                                    defaults={
                                                        'pageviews': d.pageviews,
                                                        'visitors': d.visitors })
                if not created:
                    obj.pageviews = d.pageviews
                    obj.visitors = d.visitors
                    obj.save()
            except:
                pass
            current_date += timedelta(days=1)
        
        self.stdout.write('Anaytics stats successful updated for period %s to %s\n' % (start_date, end_date))