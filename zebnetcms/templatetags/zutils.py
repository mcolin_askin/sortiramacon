# -*- coding: utf-8 -*-
from django import template
from django.template import defaultfilters
from django.contrib.sites.models import Site

from django.conf import settings

register = template.Library()

def zmedia_prefix():
    """
    Returns the string contained in the setting MEDIA_URL
    """
    try:
        from django.conf import settings
    except ImportError:
        return ''
    return settings.MEDIA_URL
zmedia_prefix = register.simple_tag(zmedia_prefix)

def zdate(value, arg=None):
    """
    Use to print "Non daté" or the right date in the correct format if not None
    """
    if value is None or value is '0000-00-00' or value is '':
        return _(u'Non daté')
    if arg is None or arg is '':
        arg = "j F Y"
    return defaultfilters.date(value, arg)
register.filter(zdate)

def get_current_site():
    """
    Returns the current site
    """
    try:
        site = Site.objects.get(id=settings.SITE_ID)
    except ImportError:
        return None
    return site
get_current_site = register.simple_tag(get_current_site)

def get_all_pages():
    from cms.utils.moderator import get_page_queryset
    page_queryset = get_page_queryset(None)
    all_pages = page_queryset.published()
    return {'all_pages' : all_pages }
get_all_pages = register.inclusion_tag('templatetages/get_all_pages.html')(get_all_pages)

def render_to_html(value):
    return value
    from cms.plugins.text.utils import plugin_tags_to_user_html
    return plugin_tags_to_user_html(value, [], "")
register.filter(render_to_html)