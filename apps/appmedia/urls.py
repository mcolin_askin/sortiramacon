"""static serving of app media for dev server.

To use this you should add the following lines to the _BOTTOM_ of your
site level urls.py file:

    if settings.DEBUG: # assuming dev server
        urlpatterns += patterns('', r'^', include('appmedia.urls'))

"""
from appmedia.templatetags import appmedia
from django.conf.urls.defaults import patterns

pats = [ ('^%s(?P<path>.*)$' % url, 'serve', {'document_root': root} )
         for url, root in appmedia.app_media_urls ]

urlpatterns = patterns('django.views.static', *pats)
