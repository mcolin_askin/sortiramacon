import re
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from django.utils.translation import ugettext_lazy as _
from cms.plugins.picture.models import Picture
from django.conf import settings

from sorl.thumbnail import get_thumbnail

class PicturePlugin(CMSPluginBase):
    model = Picture
    name = _("Picture")
    render_template = "cms/plugins/picture.html"
    text_enabled = True
    
    def render(self, context, instance, placeholder, extra=None):
        if instance.url:
            link = settings.dbgettext(instance.url)
        elif instance.page_link:
            link = instance.page_link.get_absolute_url()
        else:
            link = ""
        instance.alt = settings.dbgettext(instance.alt)
        image = instance.image
        if extra is not None:
            try:
                width = re.search(r"width=['\"]([^'\"]+)['\"]", extra).groups()[0]
                width = re.sub("px", "", width)
            except Exception, e:
                try:
                    width = re.search(r"width: ?([0-9]+)", extra).groups()[0]
                except Exception, e:
                    width = None
            try:
                height = re.search(r"height=['\"]([^'\"]+)['\"]", extra).groups()[0]
                height = re.sub("px", "", height)
            except:
                height = None
                try:
                    height = re.search(r"height: ?([0-9]+)", extra).groups()[0]
                except Exception, e:
                    height = None
            print width, height
            if width:
                context.update({ 'width':width })
                
            if height:
                context.update({ 'height':height })
            
            if width and height:
                try:
                    image = get_thumbnail(instance.image, 
                                         '%sx%s' % (width, height), 
                                         crop='center', 
                                         quality=99)
                except:
                    image = instance.image
        
        
        
        context.update({
            'picture':instance,
            'image': image,
            'link':link, 
            'placeholder':placeholder
        })
        return context 
    
    def icon_src(self, instance):
        # TODO - possibly use 'instance' and provide a thumbnail image
        return instance.image.url
        return settings.CMS_MEDIA_URL + u"images/plugins/image.png"
 
plugin_pool.register_plugin(PicturePlugin)
